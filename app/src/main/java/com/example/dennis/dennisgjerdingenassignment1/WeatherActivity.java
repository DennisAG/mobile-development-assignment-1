package com.example.dennis.dennisgjerdingenassignment1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

/**
 * - Starts when it is called from MyActivity
 * - Gets the name from the previous activity
 * - Get the coordinates from the devices GPS-censor
 * - Cuts the decimals of the coordinates got form the GPS-censor, both for easier readability
 *      for the user, and so that www.openweathermap.com can handle the coordinates.
 * - Calls other class to get data from www.openweathermap.com, this is because the class that
 *      calls the web needs to be Async. The framework of the DownloadWeatherData, is very similar
 *      to the code used in https://bitbucket.org/gtl-hig/imt3662_file_download, but I have made
 *      of adjustments in the code itself to make it fit my purpose.
 * - Saves users name, latitude and longitude in the database.
 */

public class WeatherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        /**
         * Gets the name from the previous activity
         * The framework of the code on the next 4 lines is quite similar to the one used in https://www.youtube.com/watch?v=45gq0Q8GFMM
                but used in a different way
        */
        Intent callingActivity = getIntent();
        String usersName = callingActivity.getExtras().getString("key");
        TextView weatherMessage = (TextView) findViewById(R.id.weatherMessage);
        weatherMessage.append(" " + usersName);

        /**
         * Gets the location from the GPS. All copied from:
         * http://stackoverflow.com/questions/2227292/how-to-get-latitude-and-longitude-of-the-mobiledevice-in-android
         */
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();

        //double latitude = 10.8;
        //double longitude = 50.69;
        Log.i("Test 1", Double.toString(longitude));
        DecimalFormat df = new DecimalFormat("#.##");
        DecimalFormat df2 = new DecimalFormat("#.#");
        String lon = df2.format(longitude);
        String lat = df.format(latitude);
        Log.i("Test 2", lon);

        weatherMessage.append(". You are currently at longitude: " + lon + " and latitude: " + lat);

        String url = "http://openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&APPID=c2812ee694bfcd7a9438558f02ccf182";

        try {
            new DownloadWeatherData().execute(new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        DBHelper dbHelper = new DBHelper(this);
        final ContentValues values = new ContentValues();
        values.put(DBHelper.NAME, usersName);
        values.put(DBHelper.LAT, latitude);
        values.put(DBHelper.LON, longitude);

        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.insert(DBHelper.TABLE_NAME, null, values);
        db.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.weather, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Opens a connection using the http-protocol.
     * This code is a copy from: https://bitbucket.org/gtl-hig/imt3662_file_download
     * @param url: URL of the desired destination to open a connection to.
     * @return: Resturns a inputStream
     * @throws IOException if try ddin't work.
     */

    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }

        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return in;
    }

    /**
     * This class is a copy from https://bitbucket.org/gtl-hig/imt3662_file_download
     * The downloadtext method get the JSON-object in pure text from openweathermap. This method is
     *      a direct copy from link above.
     * I filled the onPostExecute-method with my own code. This creates the JSON-object, gets
     *      relevant data from it, and shows it to the user. This method is called from the onCreate
     *      method in the weather-class.
     */

    private class DownloadWeatherData extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... urls) {
            assert urls.length == 1;
            return downloadText(urls[0]);
        }

        /**
         * - Creates JSON-object
         * - Get data from the JSON-object
         * - Fills a TextView with the data.
         * Some of the ways to get data from a JSON-object have I got from http://www.todroid.com/creating-a-weather-app-in-android-studio/
         *      The rest I figured out.
         * @param result: The JSON-object in String-format.
         */
        @Override
        protected void onPostExecute(String result) {
            JSONObject weatherObj = null;
            try {
                weatherObj = new JSONObject(result);
                if (weatherObj != null) {
                    try {
                        TextView weatherMessage = (TextView) findViewById(R.id.weatherMessage);
                        String place = weatherObj.getString("name");
                        double trueTemp = weatherObj.getJSONObject("main").getDouble("temp") - 273.15;
                        DecimalFormat decfor = new DecimalFormat("#.#");
                        String temp = decfor.format(trueTemp);
                        String weather = weatherObj.getJSONArray("weather").getJSONObject(0).getString("description");
                        double humidity = weatherObj.getJSONObject("main").getDouble("humidity");
                        double windspeed = weatherObj.getJSONObject("wind").getDouble("speed");
                        double airpressure = weatherObj.getJSONObject("main").getDouble("pressure");
                        weatherMessage.append(". This position is named " + place + ". It is now " + weather + " at this location. The temperature is: " + temp + " °C and humidity is " + humidity + "%. Windspeed is " + windspeed + "m/s and Airpressure is " + airpressure);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * - Calls openHttpConnection-method to get a connection to the URL.
     * - Reads text from the connection to that website.
     * - Returns the text as one String to the called method.
     * @param url: URL that text is got from.
     * @return: String with what retrieved from the URL.
     */
    private String downloadText(URL url) {
        int BUFFER_SIZE = 2000;
        InputStream in = null;
        try {
            in = openHttpConnection(url);
        } catch (IOException e1) {
            e1.printStackTrace();
            return "";
        }

        InputStreamReader isr = new InputStreamReader(in);
        int charRead;

        String returnString = "";
        char[] inputBuffer = new char[BUFFER_SIZE];
        try {
            while ((charRead = isr.read(inputBuffer))>0)
            {
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                returnString += readString;
                inputBuffer = new char[BUFFER_SIZE];
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return returnString;
    }
}
