package com.example.dennis.dennisgjerdingenassignment1;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;


/** This is the main activity that runs when the app starts up
 * It has 3 variables name, lon, lat, that stores data that is shown to the user when the app starts up
 * They are the name of the user and the user's last location.
 *
 * The activity doesn't do much, just prints last location that is stored in the database, and listens to button if the user
 *  clicks on it, then send send the user's name to the WeatherActivity
 */
public class MyActivity extends Activity {

    private String name = null;     // Stores the name of the user gotten from the database
    private String lon;              // Stores the longitude of the last location to the user, gotten from the database
    private String lat;              // Stores the latitude of the last location to the user, gotten from the database


    /**
     * Runs as soon the activity starts.
     * Gets name, and last location from the database
     * Prints data to the users
     * If name is null, then it's the first time the app starts
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        DBHelper dbHelper = new DBHelper(this);
        getLastLocation(dbHelper);

        if (name != null) {
            TextView helloMessage = (TextView) findViewById(R.id.helloMessage);
            helloMessage.append(" " + name + ". Your last location was Latitude: " + lat + " and Longitude: " + lon);
            EditText usersName = (EditText) findViewById(R.id.usersName);
            usersName.setText(name);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Runs when user clicks on the Get Weather Button
     * Creates intent and calls the weather-activity.
     * Sends the users name to the WeatherActivity
     * Learned how to do this from https://www.youtube.com/watch?v=45gq0Q8GFMM,
     *      so the framework of it is from there.
     * @param view: The view from activity_my.xml
     */
    public void onGetWeatherClick(View view) {

        Intent getWeatherIntent = new Intent(this, WeatherActivity.class);

        final int result = 1;

        EditText usersNameEditText = (EditText) findViewById(R.id.usersName);
        String usersName = String.valueOf(usersNameEditText.getText());

        getWeatherIntent.putExtra("key", usersName);
        startActivityForResult(getWeatherIntent, result);
    }

    /**
     * Calls the database and gets the name, longitude and latitude that was last inserted there,
     *  means users last location.
     * Then runs through the data,
     * Then sets this class's data with the data from the database
     * The name of the colomns is got from DBHelper-class and is stored there.
     * The framework of this have I got from: http://www.techotopia.com/index.php/An_Android_Studio_SQLite_Database_Tutorial
     * But they use other data and other database-setup, so not the same data.
     *
     * @param dbHelper: The database
     */

    public void getLastLocation(final DBHelper dbHelper) {
        String query = "SELECT " + DBHelper.NAME + ", " + DBHelper.LAT + ", "
                + DBHelper.LON + " FROM " + DBHelper.TABLE_NAME +
                " WHERE " + DBHelper.ID + " = (SELECT MAX(" + DBHelper.ID + ")"
                + " FROM " + DBHelper.TABLE_NAME + ")";

        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            this.name = cursor.getString(0);
            DecimalFormat df = new DecimalFormat("#.##");
            DecimalFormat df2 = new DecimalFormat("#.#");
            this.lat = df.format(cursor.getDouble(1));
            this.lon = df.format(cursor.getDouble(2));
            cursor.close();
        }
    }

}
