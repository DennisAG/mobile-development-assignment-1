package com.example.dennis.dennisgjerdingenassignment1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Class to manage the database.
 * Almost all od this class is from https://bitbucket.org/gtl-hig/imt3662_sql_notes/src/486e19d09acd?at=master,
 *  that is the mobile development class's code resources.
 * Difference is the name of the database, and the data at the bottom, the framework of those are also in the
 *  code that is referenced to above, but other data.
 *
 */
class DBHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "assignment1.db";
    public static final int DATABASE_VERSION = 1;

    /**
     * Sets up database helper.
     * @param ctx: The context of the activity
     */
    public DBHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Creates table
     * @param db: The database as parameter.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBHelper.CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //In case of Database-upgrade
    }


    /**
     * TABLE_NAME: The name of the table
     * ID: Primary key, autoincrement, id
     * LAT: Latitude
     * LON: Longitude
     * CREATE_TABLE: Query that creates the table that we use to store our data.
     */

    public static final String TABLE_NAME = "assignment1";
    static final String ID = "id";
    static final String NAME = "name";
    static final String LAT = "lat";
    static final String LON = "lon";
    public static final String CREATE_TABLE = "CREATE TABLE " + DBHelper.TABLE_NAME + " ("
            + DBHelper.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DBHelper.NAME + " TEXT,"
            + DBHelper.LAT + " DOUBLE,"
            + DBHelper.LON + " DOUBLE" + ");";

}
